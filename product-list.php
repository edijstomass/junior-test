<!DOCTYPE html>
<html>
<head>
	<title>Product List</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="product-list.css">

</head>
<body>
<header>
	<h1>Product List<h1>
	<input form="my-form" type="submit" value="Remove products" id="mass-delete" name="remove-products">
</header>

<div class="container">
	<form action="product-list.php" method="post" id="my-form">
		<div class="row">

		    <?php 		
		    	  // *obtain all product data from database
		    	include 'queryFunctions.php';
		    	$sql = "SELECT * FROM Product";
		    	$result = selectData($sql);

		    		// checks if there is any row in database
		    		// if there is, then fetch all data from database
		    	if($result != "zero"){
					 while($row = $result->fetch_assoc()){
						echo '<div class="col-sm-3 product-list">';
				    	echo '<ul>';
				    	echo '<ul class="product-name">'. $row['name'] . '</ul>';
				    	echo '<ul class="product-info">'."- " . $row['price'] . '$</ul>';
				    	echo '<ul class="product-info">'."- " . $row['unit'] . '</ul>';
				    	echo '<ul class="product-info">'."- " . $row['SKU'] . '</ul>';
				    	echo '</ul>';
				    	echo '<input type="checkbox" value="'. $row['id'] .'" id="check-box" name="boxx[]">';
				    	echo '</div>';
					 	}  
				}else{
					 // $message = "No data in database"; // alert if no data
					 // echo "<script type='text/javascript'>alert('$message');</script>";
				}
		     ?>
		
  		</div> 
  	</form>
</div> 

		<!-- When "Remove products" clicked -->
	<?php 
		if(isset($_POST['remove-products']))
		{	
			//obtain all checked checkboxes
			$allCheckbox = $_POST['boxx'];

				//and for each checked checkbox - execute query (delete product from database)
				foreach ($allCheckbox as $box)
				{ 
	    		$sql = 'DELETE FROM Product WHERE id = '.(int)$box.'';
	    		ExecuteQuery($sql,"Products removed successfully");
				}
			// refresh the page
			echo "<meta http-equiv='refresh' content='0'>";  
		}
	 ?>

</body>
</html>