<?php 

class Product
		{
			private $name;
			private $unit;
			private $price;
			private $SKU;
			private $type;
			
			function __construct($name, $unit, $price, $SKU, $type)
			{
				$this->setName($name);
				$this->setUnit($unit);
				$this->setPrice($price);
				$this->setSKU($SKU);
				$this->setType($type);
			}


			function getName(){
				return $this->name;
			}
			function setName($name){
				if($name != '' && strlen($name) <= 50){
					$this->name = $name;
				}
				else{
					echo "Invalid product Name <br>";
				}
			}


			function getUnit(){
				return $this->unit;
			}
			function setUnit($unit){
				if($unit != '' && strlen($unit) <= 50){
					$this->unit = $unit;
				}
				else{
					echo "Invalid product Unit <br>";
				}
			}


			function getPrice(){
				return $this->price;
			}
			function setPrice($price){
				if($price != '' && strlen($price) <= 50){
					$this->price = $price;
				}
				else{
					echo "Invalid product Price <br>";
				}
			}


			function getSKU(){
				return $this->SKU;
			}
			function setSKU($SKU){
				if($SKU != '' && strlen($SKU) <= 50){
					$this->SKU = $SKU;
				}
				else{
					echo "Invalid product SKU <br>";
				}
			}


			function getType(){
				return $this->type;
			}
			function setType($type){
				if($type != '' && strlen($type) <= 50){
					$this->type = $type;
				}
				else{
					echo "Invalid product type <br>";
				}
			}

}










