	productTypeObject = {
		types : ["Perfume", "Furniture", "Storage", "Book", "Other"]
	};

		// creates new option for each product type in product-add form
	for (var i = 0; i < productTypeObject.types.length; i++) {
		$("#list").append('<option value="'+ (i+1) +'">'+productTypeObject.types[i]+'</option>');
	}
		// after selecting product type, changes additional information about product type
	$('#list').change(function() {
				    if ($(this).val() === '1') {
				        $('#type-description').html('Please provide perfume bottle size<br> Example: 100ml');
				    }else if($(this).val() === '2') {
				        $('#type-description').html('Please provide furniture dimension(Length, width, and height) <br> Example: 55x105x200');
				    }else if($(this).val() === '3') {
				        $('#type-description').html('Please provide storage capacity <br> Example: 250GB');
				    }else if($(this).val() === '4') {
				        $('#type-description').html('Please provide number of pages<br> Example:1255 pages' );
				    }else if($(this).val() === '5') {
				        $('#type-description').html('Please provide any other specific product attribute (weight etc.) <br> Example: 4,7kg');
				    }
				});

