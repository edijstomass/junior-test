<!DOCTYPE html>
<html>
<head>
	<title>Product Add</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="product-add.css">
</head>
<body>	

<h1>Product Add</h1>
<hr>

<div class="container">
		<!-- Product form -->
	<form action="product-add.php" method="post">
		<div id="inputs">
		<span>SKU: </span>   <input type="text" name="sku" required maxlength="50"> <br>
		<span>Name: </span>  <input type="text" name="name" required maxlength="50"> <br> 
		<span>Price: </span> <input type="number" step="0.01" name="price" required maxlength="20"> <br>
		</div>	

		<div id="type-switcher">
			Type Switcher:  <select id="list" name="typeName"></select> <br>
			Unit: 			<input type="text" name="unit" required maxlength="50" placeholder="Quantity and unit">
					<p id="type-description">
						Please provide perfume bottle size<br>
						Example: 100ml
					</p>
			</div>

			<input type="submit" name="submitted" class="button">
		</form>
</div>
			<!-- *retrieve product data from form
				 *insert product data to database -->
		<?php
			if(isset($_POST['submitted']))
			{
				include 'productClass.php'; // Product object class
				$productObj = new Product ($_POST['name'],
										   $_POST['unit'],
										   $_POST['price'],
										   $_POST['sku'],
										   $_POST['typeName']);

				include 'queryFunctions.php';
				$sql = "INSERT INTO Product
										  (name, typeID, unit, price, SKU) 
									VALUES 
										   ('{$productObj->getName()}',
											'{$productObj->getType()}',
											'{$productObj->getUnit()}',
											'{$productObj->getPrice()}',
											'{$productObj->getSKU()}')";

				$productInserted = "The product has been inserted successfully";
				$message = ExecuteQuery($sql, "<script type='text/javascript'>alert('$productInserted');</script>");
				echo $message;
			}
		 ?>

	<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous">
	</script>

   <!-- *collects all product types
		*append product types to form (Type switcher options)
		*after selecting product type, changes additional information about product type -->
	<script type="text/javascript" src="typeSwitcher.js"></script>


</body>
</html>