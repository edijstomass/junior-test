<?php 
include 'db_connection.php';

function selectData($sql)
{
  $conn = OpenCon();
    $result = $conn->query($sql);
    
    if($result)
    {
        if($result->num_rows > 0)
        {
          return $result;
        }
        else
        {
          return "zero";
        }
    }
    else
    {
      return $result->error;
    }
}


function ExecuteQuery($sql,$name)   //$sql -> SQL query which you would like to execute.
{
    $conn = OpenCon();
    if ($conn->query($sql) === TRUE) 
    {
          return $name;        // $name -> Message which you like to see when your query is executed.
    } 
    else 
    {
      $error = "Error upgrading table:  " . $conn->error;
      CloseCon($conn);
          return $error;
    }
}